Please use EditorConfig ide plug-in for consistent development environment.

NodeJS / NPM are required.

```shell
# to setup the project, install dependencies
# - browserify
# - watchify
npm install

# to build the bundle
npm run build

# it is possible to build the sass files and the ts files independently:
npm run build-sass
npm run build-ts

# to watch sources to automatically build the bundle
# you still need your own server
npm run watch

# to watch sources and start a live-reload server
# note: the bundle is built in live but not generated on disk.
#       you still need to build it before releasing it.
npm run serve
```

- `public`: all files that are served as-is, and files that have been built
- `scss`: scss files - they are built using sass into public/css
- `src`: typescript sources - they are built into public/js/bundle.js
- `public/assets`: UI and Game assets like images, sounds, ...
- `public/css`: stylesheets for the HTML UI - generated from scss files
- `public/js`: third-party libraries (create.min.js, ...) and resulting bundle
- `public/manifests`: manifests files for the preloader

Everything is splitted in two parts: UI and Game.
UI referes to the GUI (the index.html home page, the menus, ...)
and to user interactions (clicks, key press, ...)

The Game is the typescript game itself.
