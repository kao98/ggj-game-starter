import Audio from './Game/audio'
import Game from './Game/game'
import Loader from './Loader/loader'
import UI from './UI/ui'

declare global {
  interface Window { loader: Loader, game: Game, ui: UI }
}

window.onload = (): void => {
  const DEBUG = true
  let debugInfoElement: HTMLDivElement = null

  if (DEBUG) {
    debugInfoElement = window.document.querySelector('#debugInfo')
  }

  /* eslint-disable sort-vars */
  // eslint-disable-next-line one-var
  const
    loader: Loader = new Loader(),
    audio = new Audio(loader),
    game = new Game(loader, audio, debugInfoElement),
    ui = new UI(window.document, loader, game, audio)
  /* eslint-enable sort-vars */

  if (DEBUG) {
    window.loader = loader
    window.game = game
    window.ui = ui
  }
}
