import BaseMenu from './base-menu'

export default class ScoreMenu extends BaseMenu {
  protected readonly selector: string = '#pauseScreen'

  protected setupMenu (): void {
    this.addMenuItem('resume', this.resumeButton)
    this.addMenuItem('quit', this.quitButton)
  }

  get resumeButton (): HTMLElement {
    return this.get('#resumeButton')
  }

  get quitButton (): HTMLElement {
    return this.get('#quitButton')
  }
}
