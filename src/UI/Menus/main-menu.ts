import BaseMenu from './base-menu'

export default class MainMenu extends BaseMenu {
  protected readonly selector: string = '#mainScreen'

  protected setupMenu (): void {
    this.addMenuItem('start', this.startButton)
    this.addMenuItem('about', this.aboutButton)
    this.addMenuItem('scores', this.scoresButton)
    this.addMenuItem('toggleMute', this.muteButton)
  }

  toggleMuteLabel (): void {
    let label = 'Mute'

    if (this.muteButton.innerText === label) {
      label = 'Unmute'
    }

    this.muteButton.innerText = label
  }

  get startButton (): HTMLElement {
    return this.get('#startButton')
  }

  get aboutButton (): HTMLElement {
    return this.get('#aboutButton')
  }

  get scoresButton (): HTMLElement {
    return this.get('#scoresButton')
  }

  get muteButton (): HTMLElement {
    return this.get('#muteMainButton')
  }
}
