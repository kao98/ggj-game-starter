import BaseMenu from './base-menu'

export default class AboutMenu extends BaseMenu {
  protected readonly selector: string = '#aboutScreen'

  protected setupMenu (): void {
    this.addMenuItem('close', this.closeButton)
  }

  get closeButton (): HTMLElement {
    return this.get('#closeAboutButton')
  }
}
