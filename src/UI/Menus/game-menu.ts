import BaseMenu from './base-menu'

export default class GameMenu extends BaseMenu {
  protected readonly selector: string = '#gameScreen'

  protected setupMenu (): void {
    this.addMenuItem('pause', this.pauseButton)
    this.addMenuItem('toggleMute', this.muteButton)
  }

  toggleMuteLabel (): void {
    let label = 'Mute'

    if (this.muteButton.innerText === label) {
      label = 'Unmute'
    }

    this.muteButton.innerText = label
  }

  get pauseButton (): HTMLElement {
    return this.get('#pauseButton')
  }

  get muteButton (): HTMLElement {
    return this.get('#muteGameButton')
  }
}
