import BaseMenu from './base-menu'

export default class ScoreMenu extends BaseMenu {
  protected readonly selector: string = '#scoresScreen'

  setupMenu (): void {
    this.addMenuItem('close', this.closeButton)
  }

  get closeButton (): HTMLElement {
    return this.get('#closeScoresButton')
  }
}
