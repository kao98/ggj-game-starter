interface IMenuItem {
  [key: string]: HTMLElement
}

export default abstract class BaseMenu {
  private cachedElement: HTMLElement
  private menuItems: IMenuItem
  protected abstract readonly selector: string
  protected abstract setupMenu(): void

  constructor (private readonly document: HTMLDocument) {
    this.menuItems = {}

    this.setupMenu()
  }

  getElement (): HTMLElement {
    return this.document.querySelector(this.documentSelector)
  }

  addMenuItem (name: string, element: HTMLElement): void {
    this.menuItems[name] = element
  }

  get (selector: string): HTMLElement {
    this[`_${selector}`] =
      this[`_${selector}`] || this.document.querySelector(selector)

    return this[`_${selector}`]
  }

  show (): void {
    this.element.style.display = 'block'
  }

  hide (): void {
    this.element.style.display = 'none'
  }

  on (menuItem: string, callback: () => void): void {
    this.menuItems[menuItem].addEventListener(
      'touchend',
      (event: TouchEvent) => {
        event.preventDefault()
        callback()
      }
    )

    this.menuItems[menuItem].addEventListener('click', callback)
  }

  get element (): HTMLElement {
    this.cachedElement = this.cachedElement || this.getElement()

    return this.cachedElement
  }

  get documentSelector (): string {
    return this.selector
  }
}
