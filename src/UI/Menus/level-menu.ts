import BaseMenu from './base-menu'

export default class LevelMenu extends BaseMenu {
  protected readonly selector: string = '#levelSelectionScreen'

  protected setupMenu (): void {
    this.addMenuItem('start', this.startButton)
    this.addMenuItem('cancel', this.cancelButton)
  }

  get startButton (): HTMLElement {
    return this.get('#startLevelButton')
  }

  get cancelButton (): HTMLElement {
    return this.get('#cancelLevelSelectionButton')
  }
}
