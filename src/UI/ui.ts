import Audio from '../Game/audio'
import Game from '../Game/game'
import InputsManager from './inputs-manager'
import Loader from '../Loader/loader'
import MenuManager from './menu-manager'

export default class UI {
  private menuManager: MenuManager
  private inputsManager: InputsManager
  private progressBarElement: HTMLElement

  constructor (
    private readonly document: HTMLDocument,
    private readonly loader: Loader,
    private readonly gameInstance: Game,
    private readonly audio: Audio
  ) {
    this.menuManager = new MenuManager(this.document)
    this.inputsManager = new InputsManager(this.document)

    this.gameInstance.setCanvas(this.document.querySelector('#mainCanvas'))
    this.gameInstance.setTicker()

    this.setup()
  }

  setup (): void {
    this.setupMenus()
    this.setupInputs()

    this.pauseGameWhenLosingFocus()
    this.resizeCanvasOnResize()
    this.loadUI()
  }

  setupMenus (): void {
    this.menuManager.setupMenus()

    this.menuManager.on('start', (level: unknown) => this.start(level))
    this.menuManager.on('pause', () => this.pause())
    this.menuManager.on('resume', () => this.resume())
    this.menuManager.on('quit', () => this.quit())
    this.menuManager.on('toggleMute', () => UI.toggleMute())
  }

  setupInputs (): void {
    this.inputsManager.on('keydown', (key: string) => this.handleKeydown(key))
    this.inputsManager.on('keyup', (key: string) => this.handleKeyup(key))

    this.inputsManager.on(
      'mousedown',
      (
        button: number,
        pointerX: number,
        pointerY: number,
        buttons: number
      ) => this.handleMousedown(button, pointerX, pointerY, buttons)
    )

    this.inputsManager.on(
      'mouseup',
      (
        button: number,
        pointerX: number,
        pointerY: number,
        buttons: number
      ) => this.handleMouseup(button, pointerX, pointerY, buttons)
    )

    this.inputsManager.on(
      'mousemove',
      (
        pointerX: number,
        pointerY: number,
        buttons: number
      ) => this.handleMousemove(pointerX, pointerY, buttons)
    )

    this.inputsManager.on(
      'touchmove',
      (
        touches: TouchList,
        changedTouches: TouchList
      ) => this.handleTouchmove(touches, changedTouches)
    )
  }

  pauseGameWhenLosingFocus (): void {
    return
    this.document.addEventListener(
      'visibilitychange',
      () => {
        if (this.document.hidden && this.gameStarted) {
          this.menuManager.pauseGame()
        }
      },
      false
    )

    this.document.addEventListener(
      'blur',
      () => {
        if (this.gameStarted) {
          this.menuManager.pauseGame()
        }
      }
    )
  }

  resizeCanvasOnResize (): void {
    window.addEventListener(
      'resize',
      () => {
        if (this.gameStarted) {
          this.gameInstance.resizeCanvas()
        }
      },
      false
    )
  }

  loadUI (): void {
    this.showProgress()
    this.loadUiAssets().then(() => {
      if (localStorage.getItem('muted')) {
        this.menuManager.toggleMute()
      }

      this.audio.playMenuTheme()
      this.hideProgress()
      this.menuManager.showMainMenu()
    })
  }

  // Waiting for level management to be added
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  start (level?: unknown): void {
    this.audio.stopMenuTheme()
    this.gameInstance.start(level)
  }

  pause (): void {
    this.gameInstance.pause()
  }

  resume (): void {
    this.gameInstance.resume()
  }

  quit (): void {
    this.gameInstance.stop()
    this.audio.playMenuTheme()
  }

  static toggleMute (): void {
    if (Audio.muted) {
      Audio.unmute()
      localStorage.removeItem('muted')
    } else {
      Audio.mute()
      localStorage.setItem('muted', 'muted')
    }
  }

  handleKeydown (key: string): void {
    switch (key.toLowerCase()) {
    case 'escape':
      break
    default:
      if (!this.gameRunning) {
        return
      }

      this.gameInstance.handleKeydown(key)
    }
  }

  handleKeyup (key: string): void {
    switch (key.toLowerCase()) {
    case 'escape':
      if (!this.gameStarted) {
        return
      }

      if (this.gamePaused) {
        this.menuManager.resumeGame()

        return
      }

      this.menuManager.pauseGame()

      break
    default:
      if (!this.gameRunning) {
        return
      }

      this.gameInstance.handleKeyup(key)
    }
  }

  handleMousedown (
    button: number,
    pointerX: number,
    pointerY: number,
    buttons: number
  ): void {
    if (!this.gameRunning) {
      return
    }

    this.gameInstance.handleMousedown(button, pointerX, pointerY, buttons)
  }

  handleMouseup (
    button: number,
    pointerX: number,
    pointerY: number,
    buttons: number
  ): void {
    if (!this.gameRunning) {
      return
    }

    this.gameInstance.handleMouseup(button, pointerX, pointerY, buttons)
  }

  handleMousemove (pointerX: number, pointerY: number, buttons: number): void {
    if (!this.gameRunning) {
      return
    }

    this.gameInstance.handleMousemove(pointerX, pointerY, buttons)
  }

  handleTouchmove (touches: TouchList, changedTouches: TouchList): void {
    if (!this.gameRunning) {
      return
    }

    this.gameInstance.handleTouchmove(touches, changedTouches)
  }

  showProgress (): void {
    this.progressBar.style.display = 'block'
  }

  hideProgress (): void {
    this.progressBar.style.display = 'none'
  }

  loadUiAssets (): Promise<void> {
    return this.loader
      .setProgressCallback((
        loaded: number,
        progress: number,
        total: number
      ): void => this.updateProgress(loaded, progress, total))
      .loadManifest('manifests/ui.json')
  }

  updateProgress (loaded: number, progress: number, total: number): void {
    console.log(`Progression: ${loaded}/${total}`)

    if (!this.progressBar) {
      return
    }

    // eslint-disable-next-line no-magic-numbers
    this.progressBar.style.width = `${Math.round(progress * 100)}%`
  }

  get gameStarted (): boolean {
    return this.gameInstance.isStarted
  }

  get gamePaused (): boolean {
    return this.gameStarted && this.gameInstance.isPaused
  }

  get gameRunning (): boolean {
    return this.gameStarted && !this.gamePaused
  }

  get progressBar (): HTMLElement {
    this.progressBarElement =
      this.progressBarElement || this.document.querySelector('#progressBar')

    return this.progressBarElement
  }
}
