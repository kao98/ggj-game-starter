import IListener from './ilistener'

interface IKeyPressedCollection {
  [key: string]: boolean
}

export default class InputsManager {
  private listeners: IListener
  private keyPressed: IKeyPressedCollection

  constructor (private readonly document: HTMLDocument) {
    this.listeners = {
      keydown: [],
      keyup: [],
      mousedown: [],
      mousemove: [],
      mouseup: [],
      touchend: [],
      touchmove: [],
      touchstart: []
    }

    this.keyPressed = {}

    this.setHandlers()
  }

  setHandlers (): void {
    this.setKeyboardHandlers()
    this.setTouchHandlers()
    this.setMouseHandlers()
  }

  setKeyboardHandlers (): void {
    this.document.addEventListener(
      'keydown',
      (event: KeyboardEvent) => this.keydownHandler(event.key)
    )

    this.document.addEventListener(
      'keyup',
      (event: KeyboardEvent) => this.keyupHandler(event.key)
    )
  }

  setTouchHandlers (): void {
    this.document.addEventListener(
      'touchstart',
      (event: TouchEvent) => {
        this.touchstartHandler(event.touches, event.changedTouches)
        event.preventDefault()
      }
    )

    this.document.addEventListener(
      'touchmove',
      (event: TouchEvent) => {
        this.touchmoveHandler(event.touches, event.changedTouches)
        event.preventDefault()
      }
    )

    this.document.addEventListener(
      'touchend',
      (event: TouchEvent) => {
        this.touchendHandler(event.touches, event.changedTouches)
        event.preventDefault()
      }
    )
  }

  setMouseHandlers (): void {
    this.document.addEventListener(
      'mousemove',
      (event: MouseEvent) => this.mousemoveHandler(
        event.clientX,
        event.clientY,
        event.buttons
      )
    )

    this.document.addEventListener(
      'mousedown',
      (event: MouseEvent) => this.mousedownHandler(
        event.button,
        event.clientX,
        event.clientY,
        event.buttons
      )
    )

    this.document.addEventListener(
      'mouseup',
      (event: MouseEvent) => this.mouseupHandler(
        event.button,
        event.clientX,
        event.clientY,
        event.buttons
      )
    )
  }

  keydownHandler (key: string): void {
    if (this.keyPressed[key]) {
      return
    }

    this.keyPressed[key] = true
    this.listeners.keydown.forEach((callback) => callback(key))
  }

  keyupHandler (key: string): void {
    Reflect.deleteProperty(this.keyPressed, key)
    this.listeners.keyup.forEach((callback) => callback(key))
  }

  touchstartHandler (touches: TouchList, changedTouches: TouchList): void {
    this
      .listeners
      .touchstart
      .forEach((callback) => callback(touches, changedTouches))
  }

  touchmoveHandler (touches: TouchList, changedTouches: TouchList): void {
    this
      .listeners
      .touchmove
      .forEach((callback) => callback(touches, changedTouches))
  }

  touchendHandler (touches: TouchList, changedTouches: TouchList): void {
    this
      .listeners
      .touchend
      .forEach((callback) => callback(touches, changedTouches))
  }

  mousemoveHandler (x: number, y: number, buttons: number): void {
    this
      .listeners
      .mousemove
      .forEach((callback) => callback(x, y, buttons))
  }

  mousedownHandler (
    button: number,
    x: number,
    y: number,
    buttons: number
  ): void {
    this
      .listeners
      .mousedown
      .forEach((callback) => callback(button, x, y, buttons))
  }

  mouseupHandler (button: number, x: number, y: number, buttons: number): void {
    this
      .listeners
      .mouseup
      .forEach((callback) => callback(button, x, y, buttons))
  }

  on (event: string, callback: (...args: unknown[]) => void): void {
    this.listeners[event].push(callback)
  }
}
