import AboutMenu from './Menus/about-menu'
import GameMenu from './Menus/game-menu'
import IListener from './ilistener'
import LevelMenu from './Menus/level-menu'
import MainMenu from './Menus/main-menu'
import PauseMenu from './Menus/pause-menu'
import ScoreMenu from './Menus/score-menu'

export default class MenuManager {
  private mainMenu: MainMenu
  private aboutMenu: AboutMenu
  private scoresMenu: ScoreMenu
  private levelMenu: LevelMenu
  private pauseMenu: PauseMenu
  private gameMenu: GameMenu

  private listeners: IListener

  constructor (private readonly document: HTMLDocument) {
    this.mainMenu = new MainMenu(this.document)
    this.aboutMenu = new AboutMenu(this.document)
    this.scoresMenu = new ScoreMenu(this.document)
    this.levelMenu = new LevelMenu(this.document)
    this.pauseMenu = new PauseMenu(this.document)
    this.gameMenu = new GameMenu(this.document)
    this.listeners = {
      pause: [],
      quit: [],
      resume: [],
      start: [],
      toggleMute: []
    }
  }

  setupMenus (): void {
    this
      .setupMainMenu()
      .setupAboutMenu()
      .setupScoresMenu()
      .setupLevelMenu()
      .setupPauseMenu()
      .setupGameMenu()
  }

  setupMainMenu (): MenuManager {
    this.mainMenu.on('start', () => this.selectLevel())
    this.mainMenu.on('about', () => this.showAbout())
    this.mainMenu.on('scores', () => this.showScores())
    this.mainMenu.on('toggleMute', () => this.toggleMute())

    return this
  }

  setupAboutMenu (): MenuManager {
    this.aboutMenu.on('close', () => this.showMainMenu())

    return this
  }

  setupScoresMenu (): MenuManager {
    this.scoresMenu.on('close', () => this.showMainMenu())

    return this
  }

  setupLevelMenu (): MenuManager {
    this.levelMenu.on('cancel', () => this.showMainMenu())
    this.levelMenu.on('start', () => this.startGame())

    return this
  }

  setupPauseMenu (): MenuManager {
    this.pauseMenu.on('resume', () => this.resumeGame())
    this.pauseMenu.on('quit', () => this.quitGame())

    return this
  }

  setupGameMenu (): MenuManager {
    this.gameMenu.on('pause', () => this.pauseGame())
    this.gameMenu.on('toggleMute', () => this.toggleMute())

    return this
  }

  selectLevel (): void {
    this.mainMenu.hide()
    this.levelMenu.show()
  }

  startGame (): void {
    this.levelMenu.hide()
    this.gameMenu.show()
    this.listeners.start.forEach((callback) => callback())
  }

  pauseGame (): void {
    this.gameMenu.hide()
    this.pauseMenu.show()
    this.listeners.pause.forEach((callback) => callback())
  }

  showMainMenu (): void {
    this.aboutMenu.hide()
    this.scoresMenu.hide()
    this.levelMenu.hide()
    this.mainMenu.show()
  }

  showAbout (): void {
    this.mainMenu.hide()
    this.aboutMenu.show()
  }

  showScores (): void {
    this.mainMenu.hide()
    this.scoresMenu.show()
  }

  toggleMute (): void {
    this.listeners.toggleMute.forEach((callback) => callback())
    this.mainMenu.toggleMuteLabel()
    this.gameMenu.toggleMuteLabel()
  }

  resumeGame (): void {
    this.pauseMenu.hide()
    this.gameMenu.show()
    this.listeners.resume.forEach((callback) => callback())
  }

  quitGame (): void {
    this.pauseMenu.hide()
    this.mainMenu.show()
    this.listeners.quit.forEach((callback) => callback())
  }

  on (listener: string, callback: (...args: unknown[]) => void): void {
    this.listeners[listener].push(callback)
  }
}
