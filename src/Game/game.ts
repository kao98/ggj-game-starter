import Audio from './audio'
import Loader from '../Loader/loader'

export default class Game {
  private canvas: HTMLCanvasElement
  private started: boolean
  private paused: boolean
  private stage: createjs.StageGL
  private fpsLabel: HTMLElement
  private ratio: number

  constructor (
    private readonly loader: Loader,
    private readonly audio: Audio,
    private readonly debugElement?: HTMLDivElement
  ) {
    this.canvas = null
    this.started = false
    this.paused = false
    this.ratio = 0

    if (this.debug) {
      this.fpsLabel = debugElement.querySelector('#fpsLabel')
    }
  }

  setCanvas (canvas: HTMLCanvasElement): void {
    this.canvas = canvas
  }

  initializeCanvas (): void {
    this.stage = new createjs.StageGL(this.canvas)
    this.stage.snapToPixel = false

    this.resizeCanvas()

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.stage.setClearColor('#000000')
  }

  resizeCanvas (): void {
    const VIEW_IDEAL_WIDTH = 1024

    this.canvas.width = this.canvas.parentElement.offsetWidth
    this.canvas.height = this.canvas.parentElement.offsetHeight

    if (this.stage) {
      this.stage.updateViewport(this.canvas.width, this.canvas.height)
    }

    this.ratio = this.canvas.width / VIEW_IDEAL_WIDTH
  }

  start (level?: unknown): void {
    console.log(`level: ${level}`)
    this.initializeCanvas()
    this.paused = false
    this.started = true
  }

  pause (): void {
    this.paused = true
  }

  resume (): void {
    this.paused = false
  }

  stop (): void {
    this.started = false
  }

  setTicker (): void {
    createjs.Ticker.addEventListener(
      'tick',
      (event: unknown) => this.tick(event)
    )
    createjs.Ticker.timingMode = createjs.Ticker.TIMEOUT
    createjs.Ticker.framerate = 60
  }

  // Waiting to actually do some stuff
  /* eslint-disable class-methods-use-this */
  handleKeydown (key: string): void {
    console.log(`game::handledown ${key}`)
  }

  handleKeyup (key: string): void {
    console.log(`game::handlekeyup ${key}`)
  }

  handleMousedown (
    button: number,
    x: number,
    y: number,
    buttons: number
  ): void {
    console.log(`game::handlemousedown ${button} ${x} ${y} ${buttons}`)
  }

  handleMouseup (button: number, x: number, y: number, buttons: number): void {
    console.log(`game::handlemouseup ${button} ${x} ${y} ${buttons}`)
  }

  handleMousemove (x: number, y: number, buttons: number): void {
    console.log(`game::handlemousemove ${x} ${y} ${buttons}`)
  }

  handleTouchmove (touches: TouchList, changedTouches: TouchList): void {
    console.log(`game::handletouchmove ${touches} ${changedTouches}`)
  }
  /* eslint-enable class-methods-use-this */

  tick (tickEvent: unknown): void {
    if (!this.started || this.paused) {
      return
    }

    this.stage.update()
    this.renderDebugData()
  }

  renderDebugData (): void {
    if (!this.debug) {
      return
    }

    this.fpsLabel.innerText =
      `${Math.round(createjs.Ticker.getMeasuredFPS())} fps`
  }

  get isStarted (): boolean {
    return this.started
  }

  get isPaused (): boolean {
    return this.paused
  }

  get debug (): boolean {
    return this.debugElement !== null
  }
}
