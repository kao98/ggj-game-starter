export default class Loader {
  private queue: createjs.LoadQueue
  // eslint-disable-next-line @typescript-eslint/ban-types
  private progressListener: Function

  constructor () {
    this.queue = new createjs.LoadQueue()
    this.queue.installPlugin(createjs.Sound)
    this.progressListener = null
  }

  setProgressCallback (callback: (
    loaded: number,
    progress: number,
    total: number
  ) => void): this {
    if (this.progressListener) {
      this.queue.off('progress', this.progressListener)
    }

    if (callback) {
      this.progressListener = this.queue.on(
        'progress',
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (event: any) => callback(event.loaded, event.progress, event.total)
      )
    }

    return this
  }

  loadManifest (manifest: string): Promise<void> {
    return new Promise<void>((resolve: () => void) => {
      this.queue.on('complete', resolve)
      this.queue.loadManifest({src: manifest,
        type: 'manifest'})
    })
  }
}
